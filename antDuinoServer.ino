#include "etherShield.h"
#include "ETHER_28J60.h"

#include <SPI.h>
#include <SD.h>

static uint8_t mac[6] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x24};
static uint8_t ip[4] = {10,0,0,36};
static uint16_t port = 80;
ETHER_28J60 ethernet;

char* req;
File file;

void setup()
{ 
  Serial.begin(9600);
  Serial.println("ready");
  if(!SD.begin(4))
    Serial.println("failed SD");
  ethernet.setup(mac, ip, port);
  Serial.println("set");
}

void loop()
{
  req = ethernet.serviceRequest();
  if (req)
  {
    Serial.print("req:");
    Serial.println(req);
    
    ethernet.print("<H3><a href='http://antduinoserver.tumblr.com/'>tumblr</a> |<a href='http://oedelinjean.tumblr.com/'>me</a></H3>");
    if(req[0]=='/' && (file = SD.open(req+1)))
    {
      char c[2];
      c[1]='\0';
      while(file.available())
      {
        c[0] = file.read();
        ethernet.print(c);
      }
      file.close();
    }
    else if(req[0]=='/')
    {
      ethernet.print("unable to load:");
      ethernet.print(req);
    }
    else
    {
      ethernet.print("refresh..<hr>");
      ethernet.print("i should probably watch a movie before i name my project after them");
    }
    
    ethernet.respond();
  }
  delay(100);
}

